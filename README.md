### Week 9: Regular Expressions

## Task: Extract Emails

Implement a function `extract_emails` which accepts a string as an argument and returns a list of all valid email
addresses found in the string. An email address is considered valid if it follows the following pattern:

```
username@domain.com
```

where `username` can contain letters, numbers, and periods (.) and `domain` can contain letters and periods (.).

### Example

```
>>> extract_emails("My email is john.doe@example.com")
["john.doe@example.com"]
```

You have to implement function `extract_emails(text: str) -> List[str]` in `solution.py` file.
You may create additional functions.