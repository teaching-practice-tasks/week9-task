import unittest

from solution import extract_emails


class TestSolution(unittest.TestCase):
    def test_1(self):
        test_input = "My email is john.doe@example.com"
        expected = ["john.doe@example.com"]
        self.assertEqual(expected, extract_emails(test_input))

    def test_2(self):
        test_input = "Email me at john.doe@example.com or jane.doe@example.com"
        expected = ["john.doe@example.com", "jane.doe@example.com"]
        self.assertEqual(expected, extract_emails(test_input))

    def test_3(self):
        test_input = "No email addresses here"
        expected = []
        self.assertEqual(expected, extract_emails(test_input))

    def test_4(self):
        test_input = "You can reach me at john.doe@example.com. My friend's email is jane.doe@example.com"
        expected = ["john.doe@example.com", "jane.doe@example.com"]
        self.assertEqual(expected, extract_emails(test_input))

    def test_5(self):
        test_input = "Send an email to john.doe@example.com and cc to jane.doe@example.com"
        expected = ["john.doe@example.com", "jane.doe@example.com"]
        self.assertEqual(expected, extract_emails(test_input))
